# Maestro Documentation

![Maestro logo](./docs/images/maestro-logo-large-blue.png)

Maestro documentation is built with MkDocs and ReadTheDocs. Browse the [docs/][maestro-docs-src] folder to read more about Maestro or visit the online Maestro [docs][maestro-docs-url] and [website][maestro-site-url].

## Build Docs Locally

To serve the Maestro docs on your local machine, checkout this repo and run the following commands:

**Ubuntu**

```shell
sudo apt-get install python-pip python-setuptools
pip install mkdocs
source ~/.bashrc
mkdocs serve
```

**EndeavourOS**

```shell
yay -S python-pip python-setuptools
sudo pacman -S python-pipx
pipx install mkdocs
source ~/.bashrc
mkdocs serve
```

The above commands deploy an mkdocs server on the machine's localhost on port 8000.


**Port forwarding (in case the site is served by a VM remote to your machine)**

Open a shell and type the following command to forward traffic from your local port 8000 to the VM's localhost on the same port, where the site is hosted.

```shell
ssh -L localhost:8000:localhost:8000 remoteUser@remoteServerIP
```

This step can be omitted if you have deployed the server on your machine's localhost.

## Access Docs Locally

Now, open your browser and type `https://localhost:8000` to access the main page of the Maestro docs.

[maestro-docs-src]: ./docs/
[maestro-docs-url]: https://maestro-mkdocs.readthedocs.io
[maestro-site-url]: https://themaestro.ubitech.eu/
