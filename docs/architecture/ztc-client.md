# ZTC Client Service

Maestro uses a client service to integrate with [OpenZiti][openziti-url]; an open-source secure networking platform that provides both zero-trust security and overlay networking as pure open source software.
This client is titled Zero-Trust Connectivity client, also abbreviated as `ZTC Client`.

## API

Northbound the ZTC Client listens to various Kafka topics where SONATA makes connectivity requests.
When a such message is received, the ZTC Client reads the input data from the Kafka topic and creates a corresponding connectivity service as shown in Table 1.

<center><em>Table 1: ZTC Client service API.</em></center>

| Northbound integration with | Using Kafka topic | Leads to southbound call                 | Purpose                                                   |
| :-------------------------- | :---------------- | :--------------------------------------- | :-------------------------------------------------------- |
| SONATA                      | ztc-add-service   | POST REST request to the ZTC controller  | register a new service to the ZTC fabric                  |
| SONATA                      | ztc-add-domain    | POST REST request to the ZTC controller  | register a new domain to the ZTC fabric                   |
| SONATA                      | ztc-del-service   | POST REST request to the ZTC controller  | remove an existing service from the ZTC fabric            |
| SONATA                      | ztc-del-domain    | POST REST request to the ZTC controller  | remove an existing domain from the ZTC fabric             |

<!-- References -->
[openziti-url]: https://openziti.io/
