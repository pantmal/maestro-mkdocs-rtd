# 🔖 North-Bound API Service

The Maestro ecosystem is built around multi-stakeholder and multi-role principles.
Each stakeholder role is associated with relevant Maestro services, which are made accessible to the stakeholder through a standards-compliant (based on TMForum) REST-based Northbound Interface (NBI).

## Summary of Supported APIs

Table 1 summarizes the APIs offered by Maestro and how they relate to various stakeholders and their roles.

<center><em>Table 1: Maestro TMF API families mapped to relevant stakeholders and their roles.</em></center>

| Maestro API                                            | Objective                                                                                                     |
| :------------------------------------------------------| :------------------------------------------------------------------------------------------------------------ |
| [TMF 620 Product Catalog Management API][tmf-620-url]  | Allows customers to manage products organized in catalogs and categories                                      |
| [TMF 622 Product Ordering Management][tmf-622-url]     | Allows customers to order products from the products catalog(s)                                               |
| [TMF 637 Product Inventory Management][tmf-637-url]    | Allows customers to manage the runtime of products instances                                                  |
| [TMF 633 Service Catalog Management][tmf-633-url]      | Allows service providers to manage services organized in catalogs and categories                              |
| [TMF 641 Service Ordering Management][tmf-641-url]     | Allows service providers to order services from the service catalog(s)                                        |
| [TMF 640 Service Activation Management][tmf-640-url]   | Allows service providers to activate/deactivate services                                                      |
| [TMF 638 Service Inventory Management][tmf-638-url]    | Allows service providers to manage the runtime of service instances                                           |
| [TMF 657 Service Quality Management][tmf-657-url]      | Allows service providers to describe a service's quality criteria                                             |
| [TMF 634 Resource Catalog Management][tmf-634-url]     | Allows infrastructure owners to manage resources organized in catalogs and categories                         |
| [TMF 652 Resource Ordering Management][tmf-652-url]    | Allows infrastructure owners to order resources from the service catalog(s)                                   |
| [TMF 639 Resource Inventory Management][tmf-639-url]   | Allows infrastructure owners to manage the runtime of resources instances                                     |
| [TMF 632 Party Management][tmf-632-url]                | Allows platform administrators to manage Maestro stakeholders, either individuals or organizations            |
| [TMF 669 Party Role Management][tmf-669-url]           | Allows platform administrators to assign role(s) to certain party                                             |
| [TMF 668 Partnership Management][tmf-668-url]          | Allows platform administrators to describe relations (i.e., partnership) between parties                      |
| [TMF 674 Geographic Site Management][tmf-674-url]      | Allows infrastructure owners to declare their resources' Points of Presence (PoPs) by means of abstract sites |
| [TMF 673 Geographic Address Management][tmf-673-url]   | Allows infrastructure owners to associate an abstract geographic site with specific geographic addresses      |
| [Secure Secrets' Management][vault-url]                | Allows Maestro to manage secrets (e.g., service registry credentials) in a secure and trusted way             |

The complete list of open TMF APIs can be found [online][tmf-open-apis].

## 🔖 TMF Product Management APIs

Maestro leverages TMF's Product Catalog Management API specification ([TMF620][tmf-620-url]) to describe products associated with services, which can be then organized in various products catalogs, each with one or more product categories as shown in Table 2.

<center><em>Table 2: TMF product catalog management API description.</em></center>

| Endpoint                                   | Version          | Description                                                           |
| :----------------------------------------- | :--------------- | :-------------------------------------------------------------------- |
| /tmf-api/productCatalogManagement/v4       | 4.1.0            | Provides a catalog of products                                        |
| /tmf-api/productCategoryManagement/v4      | 4.1.0            | Provides a category of products that belongs to a certain catalog     |
| /tmf-api/productCandidateManagement/v4     | 4.1.0            | Provides a candidate of products that belongs to a certain category   |
| /tmf-api/productSpecificationManagement/v4 | 4.1.0            | Provides a specification of products that maps to a certain candidate |

Once a product is stored into a product catalog, users can order it using TMF's Product Ordering API specification ([TMF622][tmf-622-url]) as shown in Table 3.
A product order may comprise one or more product specifications, thus users may buy composite products.

<center><em>Table 3: TMF product ordering management API description.</em></center>

| Endpoint                              | Version          | Description                                                                                       |
| :------------------------------------ | :--------------- | :------------------------------------------------------------------------------------------------ |
| /tmf-api/productOrderingManagement/v4 | 4.0.0            | Provides the ability to manage product orders that comprise of one or more product specifications |

Upon product ordering, a (set of) product instance(s) is/are made available to the customer.
These instances are kept in a database that follows TMF637 Product Inventory Management API specification ([TMF637][tmf-637-url]), as shown in Table 4.
This API keeps runtime information about product instances available to customers.

<center><em>Table 4: TMF product inventory management API description.</em></center>

| Endpoint                               | Version          | Description                                                           |
| :------------------------------------- | :--------------- | :-------------------------------------------------------------------- |
| /tmf-api/productInventoryManagement/v4 | 4.0.0            | Provides the ability to query and manipulate active product instances |

## 🔖 TMF Service Management APIs

Maestro leverages TMF's Service Catalog Management API specification ([TMF633][tmf-633-url]) to encode service providers' services into abstract service specifications, which can be then organized in various service catalogs, each with one or more service categories as shown in Table 5.

<center><em>Table 5: TMF service catalog management API description.</em></center>

| Endpoint                                   | Version          | Description                                                           |
| :----------------------------------------- | :--------------- | :-------------------------------------------------------------------- |
| /tmf-api/serviceCatalogManagement/v4       | 4.0.0            | Provides a catalog of services                                        |
| /tmf-api/serviceCategoryManagement/v4      | 4.0.0            | Provides a category of services that belongs to a certain catalog     |
| /tmf-api/serviceCandidateManagement/v4     | 4.0.0            | Provides a candidate of services that belongs to a certain category   |
| /tmf-api/serviceSpecificationManagement/v4 | 4.0.0            | Provides a specification of services that maps to a certain candidate |

Once a service is stored into a service catalog, users can order it using TMF's Service Ordering API specification ([TMF641][tmf-641-url]) as shown in Table 6.
A service order may comprise one or more service specifications, thus users may request composite services.

<center><em>Table 6: TMF service ordering management API description.</em></center>

| Endpoint                              | Version          | Description                                                                                       |
| :------------------------------------ | :--------------- | :------------------------------------------------------------------------------------------------ |
| /tmf-api/serviceOrdering/v4           | 4.1.0            | Provides the ability to manage service orders that comprise of one or more service specifications |

Ordered services are provisioned into a target infrastructure but remain inactive (consuming no resources) till an explicit service activation.
This is done via an explicit API call to TMF640 Service Activation Management API ([TMF640][tmf-640-url]) as shown in Table 7.

<center><em>Table 7: TMF service activation management API description.</em></center>

| Endpoint                                      | Version          | Description                                                                   |
| :-------------------------------------------- | :--------------- | :---------------------------------------------------------------------------- |
| /tmf-api/serviceActivationAndConfiguration/v4 | 4.0.0            | Provides the ability to activate/deactivate services in the service inventory |

Upon service activation, a (set of) service instance(s) is/are available in a target infrastructure.
These instances are kept in a database that follows TMF638 Service Inventory Management API specification ([TMF638][tmf-638-url]), as shown in Table 8.
This API makes runtime information about service instances available to the end users.

<center><em>Table 8: TMF service inventory management API description.</em></center>

| Endpoint                     | Version          | Description                                                           |
| :--------------------------- | :--------------- | :-------------------------------------------------------------------- |
| /tmf-api/serviceInventory/v4 | 4.0.0            | Provides the ability to query and manipulate active service instances |

Maestro implements TMF's Service Quality Management API ([TMF657][tmf-657-url]) to describe an active service's quality criteria as shown in Table 9.

<center><em>Table 9: TMF service quality management API description.</em></center>

| Endpoint                             | Version          | Description                                                               |
| :----------------------------------- | :--------------- | :------------------------------------------------------------------------ |
| /tmf-api/serviceQualityManagement/v4 | 4.0.0            | Describe a service's quality criteria                                     |

## 🔖 TMF Resource Management APIs

Maestro leverages TMF's Resource Catalog Management API specification ([TMF634][tmf-634-url]) to encode resources into abstract resource specifications, which can be then organized in various resource catalogs, each with one or more resource categories, as shown in Table 10.

<center><em>Table 10: TMF resource catalog management API description.</em></center>

| Endpoint                                    | Version           | Description                                                            |
| :------------------------------------------ | :---------------- | :--------------------------------------------------------------------- |
| /tmf-api/resourceCatalogManagement/v4       | 4.1.0             | Provides a catalog of resources                                        |
| /tmf-api/resourceCategoryManagement/v4      | 4.1.0             | Provides a category of resources that belongs to a certain catalog     |
| /tmf-api/resourceCandidateManagement/v4     | 4.1.0             | Provides a candidate of resources that belongs to a certain category   |
| /tmf-api/resourceSpecificationManagement/v4 | 4.1.0             | Provides a specification of resources that maps to a certain candidate |

Once a resource is stored into a resource catalog, users can order it using TMF's Resource Ordering API specification ([TMF652][tmf-652-url]), as shown in Table 11.
A resource order may comprise one or more resource specifications, thus users may request composite resources.

<center><em>Table 11: TMF resource order management API description.</em></center>

| Endpoint                               | Version          | Description                                                                                         |
| :------------------------------------- | :--------------- | :-------------------------------------------------------------------------------------------------- |
| /tmf-api/resourceOrderingManagement/v4 | 4.0.0            | Provides the ability to manage resource orders that comprise of one or more resource specifications |

A completed resource order results in an available resource instance.
These instances are kept in a database that follows TMF's Resource Inventory Management API specification ([TMF639][tmf-639-url]), as shown in Table 12.
This API makes runtime information about resource instances available to the end users.

<center><em>Table 12: TMF resource inventory management API description.</em></center>

| Endpoint                                | Version          | Description                                                            |
| :-------------------------------------- | :--------------- | :--------------------------------------------------------------------- |
| /tmf-api/resourceInventoryManagement/v4 | 4.0.0            | Provides the ability to query and manipulate active resource instances |

<!-- Resources are ordered out of an available resource pool. Maestro uses TMF's Resource Pool Management API specification ([TMF685][tmf-685-url]), as shown in Table 13, to oversee the available pool of underlying resources and how these resources are mapped to overlay services.

<center><em>Table 13: TMF resource pool management API description.</em></center>

| Endpoint                           | Version          | Description                                                                |
| :--------------------------------- | :--------------- | :------------------------------------------------------------------------- |
| /tmf-api/resourcePoolManagement/v1 | 1.0.0            | Provides the ability to manage resource reservation in a pool of resources | -->

## 🔖 TMF Party Management APIs

Party is an abstract concept that represents an individual (person) or an organization that has any kind of relation with the enterprise.
Maestro leverages TMF's Party Management API ([TMF632][tmf-632-url]) to encode parties and their characteristics, while TMF's Party Role Management API ([TMF669][tmf-669-url]) is used to associate a party with one or more roles.
Maestro also uses TMF's Partnership Management API ([TMF668][tmf-668-url]) to encode relations between parties.
These API endpoints are shown in Table 13.

<center><em>Table 13: TMF party, party role, and partnership management APIs description.</em></center>

| Endpoint                          | Version          | Description                                         |
| :---------------------------------| :--------------- | :-------------------------------------------------- |
| /tmf-api/party/v4/individual      | 4.0.0            | Manage individual parties                           |
| /tmf-api/party/v4/organization    | 4.0.0            | Manage corporate parties (i.e., organizations)      |
| /tmf-api/partyRoleManagement/v4   | 4.0.0            | Manage party roles                                  |
| /tmf-api/partnershipManagement/v4 | 4.0.0            | Manage partnership relations                        |

## 🔖 TMF Location Management APIs

Maestro needs to be aware of the underlying domains' locations as well as the resources offered by these domains. To do so, Maestro leverages TMF's Geographic Site Management API ([TMF674][tmf-674-url]) to encode the abstract location of a domain along with specific addresses ([TMF673][tmf-673-url]) where this domain offers resources, as shown in Table 14.
The later API is part of the TMF 674, as the geographic site data model encloses a list of geographic address data models.

<center><em>Table 14: TMF geographic site and address management API description.</em></center>

| Endpoint                             | Version          | Description                                                                                             |
| :----------------------------------- | :--------------- | :------------------------------------------------------------------------------------------------------ |
| /tmf-api/geographicSiteManagement/v5 | 5.0.0            | Manage resource locations at abstract sites, where each site may contain a list of geographic addresses |

<!-- ## 🔖 TMF SLA Management APIs

Maestro must provide certain quality guarantees to service providers.
To do so, Maestro implements TMF's SLA Management API ([TMF623][tmf-623-url]) to associate active service instances kept into the Service Inventory Management database with service-level agreement (SLA) guarantees (see Table 15).

<center><em>Table 15: TMF SLA management API description.</em></center>

| Endpoint                             | Version          | Description                                                               |
| :----------------------------------- | :--------------- | :------------------------------------------------------------------------ |
| /tmf-api/slaManagement/v2            | 2.0.0            | Associate service instances with SLA metrics that Maestro needs to ensure | -->

## 🔖 Secure Secrets' Management API

Maestro integrates with third-party service registries (e.g., GitHub, GitLab, Harbor, jFrog) to pull service images (e.g., docker containers) and descriptors (e.g., helm charts).
To do so, Maestro may require to authenticate against these registries, in case they are kept private.
For this reason, Maestro leverages [HashiCorp's Vault][vault-url] to manage the credentials for these service registries.
This service is also used by Maestro to manage Kubernetes secrets and sensitive configuration (e.g., kube config) during service deployment.
Vault's API endpoint is provided in Table 15.

<center><em>Table 15: Secrets' management API description.</em></center>

| Endpoint     | Version          | Description                                                |
| :------------| :--------------- | :----------------------------------------------------------|
| /v1/secret/  | latest           | Manage credentials and secrets in a secure and trusted way |

<!-- References -->
[tmf-open-apis]: https://www.tmforum.org/oda/open-apis/directory

[tmf-620-url]: https://tmf-open-api-table-documents.s3.eu-west-1.amazonaws.com/OpenApiTable/TMF620_Product_Catalog/4.1.0/user_guides/TMF620_Product_Catalog_Management_API_v4.1.0_specification.pdf
[tmf-622-url]: https://tmf-open-api-table-documents.s3.eu-west-1.amazonaws.com/OpenApiTable/TMF622_Product_Ordering/4.0.0/user_guides/TMF622_Product_Ordering_Management_API_v4.0.0_specification.pdf
[tmf-637-url]: https://tmf-open-api-table-documents.s3.eu-west-1.amazonaws.com/OpenApiTable/TMF637_Product_Inventory/4.0.0/user_guides/TMF637_Product_Inventory_Management_API_v4.0.0_specification.pdf

[tmf-633-url]: https://tmf-open-api-table-documents.s3.eu-west-1.amazonaws.com/OpenApiTable/TMF633_Service_Catalog/4.0.0/user_guides/TMF633_Service_Catalog_Management_API_v4.0.0_specification.pdf
[tmf-641-url]: https://tmf-open-api-table-documents.s3.eu-west-1.amazonaws.com/OpenApiTable/TMF641_Service_Ordering/4.1.0/user_guides/TMF641_Service_Ordering_Management_API_v4.1.0_specification.pdf
[tmf-640-url]: https://tmf-open-api-table-documents.s3.eu-west-1.amazonaws.com/OpenApiTable/TMF640_Service_Activation/4.0.0/user_guides/TMF640_Service_Activation_Management_API_v4.0.0_specification.pdf
[tmf-638-url]: https://tmf-open-api-table-documents.s3.eu-west-1.amazonaws.com/OpenApiTable/TMF638_Service_Inventory/4.0.0/user_guides/TMF638_Service_Inventory_Management_API_v4.0.0_specification.pdf
[tmf-657-url]: https://tmf-open-api-table-documents.s3.eu-west-1.amazonaws.com/OpenApiTable/TMF657_Service_Quality_Management/4.0.0/user_guides/TMF657_Service_Quality_Management_Management_API_v4.0.0_specification.pdf

[tmf-634-url]: https://tmf-open-api-table-documents.s3.eu-west-1.amazonaws.com/OpenApiTable/TMF634_Resource_Catalog/4.1.0/user_guides/TMF634_Resource_Catalog_Management_API_v4.1.0_specification.pdf
[tmf-652-url]: https://tmf-open-api-table-documents.s3.eu-west-1.amazonaws.com/OpenApiTable/TMF652_Resource_Order/4.0.0/user_guides/TMF652_Resource_Order_Management_API_v4.0.0_specification.pdf
[tmf-639-url]: https://tmf-open-api-table-documents.s3.eu-west-1.amazonaws.com/OpenApiTable/TMF639_Resource_Inventory/4.0.0/user_guides/TMF639_Resource_Inventory_Management_API_v4.0.0_specification.pdf
<!-- [tmf-685-url]: https://tmf-open-api-table-documents.s3.eu-west-1.amazonaws.com/Historic/TMF685_Resource_Pool_Management/3.0.0/user_guides/TMF685_Resource_Pool_Management_API_user_guides_18.0.1.pdf -->

[tmf-632-url]: https://tmf-open-api-table-documents.s3.eu-west-1.amazonaws.com/OpenApiTable/TMF632_Party/4.0.0/user_guides/TMF632_Party_Management_API_v4.0.0_specification.pdf
[tmf-669-url]: https://tmf-open-api-table-documents.s3.eu-west-1.amazonaws.com/OpenApiTable/TMF669_Party_Role/4.0.0/user_guides/TMF669_Party_Role_Management_API_v4.0.0_specification.pdf
[tmf-668-url]: https://tmf-open-api-table-documents.s3.eu-west-1.amazonaws.com/OpenApiTable/TMF668_Partnership_Type/4.0.0/user_guides/TMF668_Partnership_Management_API_v4.0.0_specification.pdf

[tmf-674-url]: https://tmf-open-api-table-documents.s3.eu-west-1.amazonaws.com/Beta/TMF674_Geographic_Site/5.0.0/user_guides/TMF674-GeographicSite-v5.0.0.pdf
[tmf-673-url]: https://tmf-open-api-table-documents.s3.eu-west-1.amazonaws.com/Beta/TMF673_Geographic_Address/5.0.0/user_guides/TMF673_Geographic_Address-v5.0.0.pdf

<!-- [tmf-623-url]: https://tmf-open-api-table-documents.s3.eu-west-1.amazonaws.com/Historic/TMF623_SLA/2.0.0/user_guides/TMF623_SLA_Management_API_user_guides_14.5.1.pdf -->

[vault-url]: https://www.vaultproject.io/
