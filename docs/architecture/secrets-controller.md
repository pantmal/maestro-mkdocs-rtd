# Secrets Controller Service

Maestro uses a dedicated service for managing user secrets. This service is based on the [Sealed Secrets][sealed-secrets-url] open-source project.

## API

Northbound the Secrets Controller listens to various Kafka topics where SONATA makes requests for secrets.
When a such message is received, the Secrets Controller reads the input data from the Kafka topic and provides the corresponding secret service as shown in Table 1.

<center><em>Table 1: Secrets controller service API.</em></center>

| Northbound integration with | Using Kafka topic | Leads to southbound call                 | Purpose                                                   |
| :-------------------------- | :---------------- | :--------------------------------------- | :-------------------------------------------------------- |
| SONATA                      | secret-X          | X  |  |
| SONATA                      | secret-Y          | Y  |  |

<!-- References -->
[sealed-secrets-url]: https://github.com/bitnami-labs/sealed-secrets
