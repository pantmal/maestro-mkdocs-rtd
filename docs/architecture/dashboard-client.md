# Dashboard Client Service

Maestro employs a `Dashboard Client` service to visualize the monitoring data that is aggregated at the central `management cluster` via Maestro's telemetry capabilities.
To do so, Maestro offers a `Dashboard-aaS` service specification that can be ordered at the central `management cluster`.
This service provisions a [Grafana][grafana-url] service at the central `management cluster` and indicates certain service metrics that needs to be visualized onto a Grafana dashboard.
Then, Maestro uses the `Dashboard Client` service to create new graphs on the central Grafana instance, based on the service metrics indicated as service characteristics in the `Dashboard-aaS` service.

## API

Northbound the `Dashboard Client` listens to a Kafka topic where SONATA makes requests for the visualization of service metrics.
When a such message is received, the `Dashboard Client` reads the input data from the Kafka topic and issues the corresponding graph creation commands to the central Grafana service as shown in Table 1.

<center><em>Table 1: Dashboard Client service API.</em></center>

| Northbound integration with | Using Kafka topic     | Leads to southbound call                   | Purpose                                              |
| :-------------------------- | :-------------------- | :----------------------------------------- | :----------------------------------------------------|
| SONATA                      | dashboard-add-graph   | POST REST request to the Grafana service   | create a graph to visualize a certain service metric |
| SONATA                      | dashboard-del-graph   | DELETE REST request to the Grafana service | delete a graph related to a certain service metric   |

<!-- References -->
[grafana-url]: https://grafana.com/oss/grafana/
