# Helm Engine Service

Maestro supports services packaged using the three (3) most prevalent package managers to date:

- Docker-based services using the [docker-compose][docker-comp-url] package manager.
- Kubernetes-native services packaged via [Kubernetes][k8s-url] manifest descriptors.
- Kubernetes-native services packaged via [Helm][helm-url] manifest descriptors.

No matter how service providers have packaged their applications, Maestro uses Helm as basic abstraction to manage user services in its core.
To do so, the Helm Engine leverages services from three Go-based libraries:

- [Helm client][helm-client-url] for managing helm services.
- [Native Helm library][helm-native-lib-url] for splitting helm charts into multiple sub-charts.
- [Kompose library][kompose-lib-url] for managing docker-to-helm and k8s-to-helm conversions.

## API

At the northbound, the Helm Engine listens to various Kafka topics where SONATA requests several Helm services.
When a message on such a topic is published by SONATA, the Helm engine reads the input data from the Kafka topic and invokes the respective handler to provide the requested service.
At the southbound, every such handler calls services of the two Helm libraries (i.e., Helm client and native helm library) or the Kompose library as shown in Table 1.

<center><em>Table 1: Helm Engine service API.</em></center>

| Northbound integration with | Using Kafka topic | Leads to southbound call | Purpose                                                                                   |
| :-------------------------- | :---------------- | :----------------------- | :---------------------------------------------------------------------------------------- |
| SONATA                      | helm-convert      | helm native library      | to convert a docker-based or Kubernetes-based service into a Helm-based service           |
| SONATA                      | helm-validate     | helm client              | to validate a Helm-based service                                                          |
| SONATA                      | helm-split        | helm client              | to split a Helm-based service into multiple sub-services for a geo-distributed deployment |
| SONATA                      | helm-identify     | helm client              | to identify the compute resources required by a Helm-based service or sub-service         |
| SONATA                      | helm-connect      | helm client              | to connect to a Kubernetes cluster                                                        |
| SONATA                      | helm-deploy       | helm client              | to deploy a service or service component on a Kubernetes cluster                          |

<!-- References -->
[docker-comp-url]: https://docs.docker.com/compose/
[k8s-url]: https://kubernetes.io/
[helm-url]: https://helm.sh/

[helm-client-url]: https://github.com/mittwald/go-helm-client
[helm-native-lib-url]: https://github.com/helm/helm
[kompose-lib-url]: https://github.com/kubernetes/kompose
