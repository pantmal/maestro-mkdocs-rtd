# Secrets' Manager Service

Maestro leverages HashiCorp's [Vault][vault-url] to store sensitive data related to the various service registries (e.g., GitLab, GitHub, jFrog, Harbor) associated with Maestro as well as sensitive files related to Maestro's access to the underlying testbed resources (e.g., kube config files).
Vault exposes a REST-based NBI that allows to manage all this sensitive data via secrets.

## API

Vault exposes REST API endpoints as shown in Table 1.

<center><em>Table 1: Secrets' Manager API.</em></center>

| Northbound integration with | Endpoint          | Purpose                                     |
| :-------------------------- | :-----------------| :-------------------------------------------|
| Service providers           | v1/secret/        | Create, Read, Update, Delete (CRID) secrets |
| Helm Engine                 | v1/secret/        | Create, Read, Update, Delete (CRID) secrets |

<!-- References -->
[vault-url]: https://www.vaultproject.io/
