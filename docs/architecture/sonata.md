# SONATA Service Order Management Service

SONATA is the heart of Maestro; this microservice uses Business Process Model and Notation (BPMN) workflow diagrams to encode the lifecycle of a service order based on the TMF service order lifecycle diagram.

SONATA leverages the [Kogito Business Automation][kogito-url] library by RedHat to encode service orders into BPMN processes following the service order state machine suggested by TMF as shown in [Figure 1](#sonata-lifecycle-so).

![TMF Service order lifecycle][sonata-lifecycle-so]
<center><em>Figure 1: TMF service order lifecycle.</em></center>

Every node in this state machine corresponds to a BPMN task, where SONATA integrates with other microservices to conduct the necessary operations for the service order to complete.

## API

Northbound SONATA listens to a Kafka topic where new service order requests are produced by the TMF Service Order service.
When a message on this topic is published by the TMF Service Order, SONATA reads the input data from the Kafka topic and bootstraps a BPMN flow diagram to accommodate the service order.
At the southbound, every state of the BPMN diagram requires services from the Helm Engine, OSS Client, and ZTC Client services as shown in Table 1.

<center><em>Table 1: SONATA service API.</em></center>

| SONATA integrates with       | Using Kafka topic | Purpose                                                                                                                                             |
| :--------------------------- | :---------------- | :-------------------------------------------------------------------------------------------------------------------------------------------------- |
| TMF Service Order            | tmf-order         | to receive a new service order request                                                                                                              |
| Helm Engine                  | helm-convert      | to convert a docker-based or Kubernetes-based service into a Helm-based service                                                                     |
| Helm Engine                  | helm-validate     | to validate a Helm-based service                                                                                                                    |
| Helm Engine                  | helm-split        | to split a Helm-based service into multiple sub-services for a geo-distributed deployment                                                           |
| Helm Engine                  | helm-identify     | to identify the compute resources required by a Helm-based service or sub-service                                                                   |
| OSS Client                   | oss-order-k8aas   | to order a Kubernetes cluster for hosting a Helm-based service or sub-service                                                                       |
| OSS Client                   | oss-order-5gaas   | to order 5G at a designated area of service                                                                                                         |
| ZTC Client                   | ztc-add-service   | to make a private service (e.g., a Kubernetes cluster API server) accessible through the ZTC fabric (not required for publicly accessible clusters) |
| ZTC Client                   | ztc-del-service   | to remove a service from the ZTC fabric                                                                                                             |
| Helm Engine                  | helm-connect      | to connect to a Kubernetes cluster (either via the ZTC fabric or publicly)                                                                          |
| Helm Engine                  | helm-deploy       | to deploy a service or sub-service on a Kubernetes cluster                                                                                          |

<!-- References -->
[sonata-lifecycle-so]: ../images/svc-order-lifecycle.png "Service order lifecycle"
[sonata-lifecycle-ss]: ../images/svc-spec-lifecycle.png "Service specification lifecycle"
[kogito-url]: https://kogito.kie.org/get-started/
