# OSS Client Service

Maestro integrates with [ETSI OpenSlice][etsi-osl-url]; a standardized Operations Support System (OSS) that provides compute and network resources as a service.
Maestro uses TMF-based APIs to consume two important services of ETSI OpenSlice to satisfy service providers:

- A Kubernetes-as-a-Service (K8aaS) service specification.
- a 5G-as-a-Service (5GaaS) service specification.

Both services are ordered by Maestro using an OSS Client service.

## API

Northbound the OSS Client listens to various Kafka topics where SONATA makes K8aaS or 5GaaS requests.
When a such message is received, the OSS Client reads the input data from the Kafka topic and creates a corresponding service order to OpenSlice as shown in Table 1.

<center><em>Table 1: OSS Client service API.</em></center>

| Northbound integration with | Using Kafka topic | Leads to southbound call                | Purpose                                                   |
| :-------------------------- | :---------------- | :-------------------------------------- | :-------------------------------------------------------- |
| SONATA                      | oss-order-k8saas  | OpenSlice TMF-based K8aaS service order | to create a Kubernetes cluster for a service deployment   |
| SONATA                      | oss-order-5gaas   | OpenSlice TMF-based 5GaaS service order | to create a 5G deployment at a designated area of service |

<!-- References -->
[etsi-osl-url]: https://osl.etsi.org/
