# Telemetry Client Service

Maestro lives in a central `management cluster` alongside with a [Prometheus][prometheus-url] service used to monitor the state of this cluster.
Maestro leverages this central Prometheus service to gather telemetry data across all `managed clusters`, where Maestro deploys user services.

To do so, Maestro requires the underlying OSS to create on-demand K8s clusters with a local cluster-wide Prometheus service in place.
Given this, Maestro uses a `Telemetry Client` service to configure the Prometheus instance on the `managed cluster` so as for the latter to publish service metrics to the central Prometheus instance.
This way, all Maestro-deployed services report their telemetry to a central Prometheus instance.

## API

Northbound the `Telemetry Client` listens to a Kafka topic where SONATA makes requests for telemetry endpoints' registration.
When a such message is received, the `Telemetry Client` reads the input data from the Kafka topic and registers the corresponding metrics from a `managed cluster`to the `central management cluster` as shown in Table 1.

<center><em>Table 1: Telemetry Client service API.</em></center>

| Northbound integration with | Using Kafka topic | Leads to southbound call                      | Purpose                                                           |
| :-------------------------- | :---------------- | :-------------------------------------------- | :---------------------------------------------------------------- |
| SONATA                      | tlm-add-metric   | POST REST request to the Prometheus service   | publish service metric(s) to the central Prometheus service         |
| SONATA                      | tlm-del-metric   | DELETE REST request to the Prometheus service | stop publishing service metric(s) to the central Prometheus service |

<!-- References -->
[prometheus-url]: https://prometheus.io/
