# Auxiliary Management Services

Maestro is a cloud-native service that lives in a `management` Kubernetes cluster.
In this cluster, popular open-source services are installed alongside Maestro offering important services to the orchestrator.
These services are summarized in the rest of this section.

## Summary of Auxiliary Management Services

Table 1 summarizes important services that surround Maestro in the `management cluster`.

<center><em>Table 1: Auxiliary open-source services to Maestro .</em></center>

| Service Name                     | Objective                                                                                                                                                             |
| :--------------------------------| :-------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Apache Kafka][kafka-url]        | Serves as a message and event bus among all Maestro services.                                                                                                         |
| [Kong Gateway][kong-url]         | A thin entry-point to the Maestro TMF APIs that dispatches input requests to the correct TMF API endpoint.                                                            |
| [Keycloak][keycloak-url]         | Authenticates users against Maestro, before allowing these users to access the Maestro Northbound APIs.                                                               |
| [Prometheus Operator][kprom-url] | A cloud-native version of the [Prometheus][prometheus-url] monitoring and alerting system with a timeseries database backend.                                         |
| [Grafana Mimir][mimir-url]       | Enhances the Prometheus operator with long-term persistence and high-availability features.                                                                           |
| [Grafana Loki][loki-url]         | A central component that aggregates, stores, and queries log entries from every Maestro microservice.                                                                 |
| [Grafana Tempo][tempo-url]       | A central component that integrates with popular open-source tracing protocols, such as OpenTelemetry, to collect and persist traces from every Maestro microservice. |
| [Grafana][grafana-url]           | An external dashboard that integrates with (i) Loki for visualizing service logs, (ii) Tempo for visualizing service traces, and (iii) Mimir for visualizing SLAs.    |
| [OpenTelemetry][otl-url]         | A protocol used to instrument, generate, collect, and export telemetry data (metrics, logs, and traces) for analyzing a software’s performance and behavior.          |
| [PostgreSQL][pg-url]             | A cloud-native relational database management system used to persist data across all Maestro microservices (e.g., the TMF schema, Helm Engine, OSS Client, etc.).     |
| [Vault][vault-url]               | A secure database to store and manage secrets of any kind (e.g., credentials, kubernetes secrets, etc.).                                                              |

<!-- References -->
[kafka-url]: https://kafka.apache.org/
[kong-url]: https://konghq.com/
[keycloak-url]: https://www.keycloak.org/
[kprom-url]: https://prometheus-operator.dev/
[prometheus-url]: https://prometheus.io/
[mimir-url]: https://grafana.com/oss/mimir/
[loki-url]: https://grafana.com/oss/loki/
[tempo-url]: https://grafana.com/oss/tempo/
[grafana-url]: https://grafana.com/oss/grafana/
[otl-url]: https://opentelemetry.io/
[pg-url]: https://www.postgresql.org/
[vault-url]: https://www.vaultproject.io/
