# Secure & Trusted Domain Management

To securely manage multiple geo-distributed domains, Maestro employs a programmable Zero-Trust Connectivity (ZTC) fabric as shown in [Figure 1](#maestro-domains).
The idea is based on the concept of software-defined perimeter (SDP), which builds a domain’s network perimeter in software, rather than hardware.
The fabric's administrator can register and authenticate infrastructure owners and allow them to expose domain infrastructure resources and services through the fabric.

To materialize this service, Maestro employs [OpenZiti][openziti-url]; an open-source secure networking platform that provides both zero-trust security and overlay networking as pure open source software.
A dedicated [ZTC service][maestro-ztc-service] is used by Maestro to interact with the ZTC network in a dynamic fashion.
A detailed description of the OpenZiti security features is available [here][openziti-security-url].

![Maestro domains][maestro-domains]
<center><em>Figure 1: Secure & trusted integration of domains with Maestro via ZTC.</em></center>

Two key services comprise the Maestro ZTC network, i.e., the ZTC Fabric service and ZTC Client service, both of which are described below.

## ZTC Fabric Service

The ZTC Fabric service establishes a programmable encrypted software-based overlay network backbone over the Internet, where various domain resources and services may register, thus rendering themselves accessible from other members of the fabric.
The ZTC Fabric is comprised of two core components that are exposed through a single domain name `https://ztc.euprojects.net` as follows:

- ZTC Fabric Portal: `https://ztc.euprojects.net:8443/login/`
- ZTC Fabric Controller: `https://ztc.euprojects.net:8441`

Both components are described below.

### ZTC Controller

The ZTC Controller acts as a configuration element of the fabric, allowing the fabric's owner to add/remove nodes and admit/discard services via APIs.

### ZTC Router

One or more ZTC routers act as forwarding elements of the ZTC network as shown in [Figure 1](#maestro-domains).

## ZTC Client Service

The ZTC Client service is a software package that new domain infrastructure owners obtain upon successful registration of their domain(s) to the Maestro portal.
This piece of software is deployed with a single command and uses an authentication token (also obtained during registration) to authenticate against the public ZTC Fabric service.
At this point the new domain becomes part of the ZTC network (i.e., part of the Maestro platform), however, no services of this domain are exposed through the fabric yet.
To do so, the domain infrastructure owner needs to issue another call to the ZTC Fabric service, asking for specific services to be added into the fabric’s network.
At that point, these services become accessible by other parties of the fabric (e.g., the remote Maestro instance).

Three ZTC Client service flavours are available in Table 1, depending on the needs or technological preferences of the domain owner.

<center><em>Table 1: ZTC Client service options.</em></center>

| ZTC Client Name         | ZTC Client Software                                                           | Description                                                                                   |
| :-----------------------| :---------------------------------------------------------------------------- | :-------------------------------------------------------------------------------------------- |
| Host tunneller          | <img alt="Docker" src="../../images/icon-docker.jpg" width="40" height="40"/> | Exposes available resources of a physical and/or virtual host.                                |
| Edge router tunneller   | <img alt="Docker" src="../../images/icon-docker.jpg" width="40" height="40"/> | Exposes available resources of multiple physical/virtual hosts in a Local Area Network (LAN). |
| K8s tunneller           | <img alt="Helm"   src="../../images/icon-helm.jpg"   width="40" height="40"/> | Exposes available resources (e.g., pods) within a Kubernetes cluster. Gets deployed as a pod. |

## ZTC Fabric Domain

One or more core ZTC routers along with the ZTC controller are deployed on a public domain (e.g., the ztc.domain.io shown in [Figure 1](#maestro-domains)).
This domain is the root of trust of the ZTC network, thus must be accessible from every managed domain.

## ZTC Managed Domains

Every managed domain that wishes to join the ZTC network must employ one or more instances of the ZTC Client service.
[Figure 1](#maestro-domains) shows an example deployment with a central management domain (where Maestro is deployed) and several underlying managed domains, where multiple OSS instances manage the domains' resources and expose them through the ZTC network.
In this setup each of the domains use a ZTC client service to connect to the ZTC Fabric service located in the trusted ZTC domain shown in gray.

## ZTC Security

Ziti networks use robust modern cryptography and security mechanisms. Each component within a Ziti network uses security technology that fits its role and use-cases.

Here are the different types of connections:

- `control` - connections between a controller and a router that manage network state. Secured via mTLS 1.2+
- `link` - connections between routers to form a mesh network. Secured via mTLS 1.2+
- `edge` - multiplexed connections between an SDK and an Edge Router that carries service connections. Secured via mTLS 1.2+
- `controller APIs` - connections between a controller and an SDK (or other software) via HTTPS and/or secure websockets. Secured via TLS, mTLS 1.2+
- `service` - connections carried by an edge connection and establish communication between a client and host. Application data is end-to-end encrypted via libsodium public private key cryptography.

Figure 2 shows the `control`, `link`, `edge`, and `controller API` connections. The `service` connections exist within an edge connection and are pictured in more detail in Figure 3.

![ZTC connections][ztc-connections]
<center><em>Figure 2: ZTC control, link, edge, and controller API connections (taken by OpenZiti online).</em></center>

Connections between SDKs and Edge Routers are called `edge` connections.
`edge` connections are multiplexed and carry multiple `service` connections.
Each connection is for a specific service and secured with end-to-end encryption in order to transport application/service data securely between the two intended parties only.

![Edge connections][ztc-svc-connections]
<center><em>Figure 3: ZTC service connections (taken by OpenZiti online).</em></center>

More details about ZTC connections and ZTC in general can be found [online][openziti-security-url].

<!-- References -->
[maestro-domains]: ../images/ztc-concept.png "Maestro domains"
[ztc-connections]: https://openziti.io/assets/files/connections-585b0f4d2d57d592e45386d4f39e7c58.png
[ztc-svc-connections]: https://openziti.io/assets/files/connections-edge-sdk-sdk-0f9f5a0ab982015fb9b67b5cfb807715.png
[maestro-ztc-service]: ../architecture/ztc-client.md
[openziti-url]: https://openziti.io/
[openziti-security-url]: https://openziti.io/docs/learn/core-concepts/security/connection-security
