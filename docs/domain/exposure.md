# Exposure of Domain Services

The Maestro platform is successfully expanded into a new domain, only when a ZTC Client service is successfully authenticated against the ZTC Fabric service.
At this point, the ZTC network owner may use the following steps to register new services to the ZTC network.

## Service Deployment

Let's assume the following network as an example:

```text
A ---- {OpenZiti} ---- B
```

- A: 10.0.2.4
- B: 192.168.5.213

Both A and B have a ziti tunneller and are connected to the openziti network.

A simple HTTP service is running on A on port 8000.

### Service Configurations

In OpenZiti each service is defined by two configurations:

- host configuration
- intercept configuration

### Host configuration

[Host configurations][host-conf] define the service from the viewpoint of the tunneller of the service. i.e., How a service is seen from inside A. In our case a simple configuration would be:

```json
{
  "address":"10.0.2.4",
  "port":8000,
  "protocol":"tcp",
}
```

Basically we define the address, the port and the protocol that the service listens to.

### Intercept configuration

[Intercept configuration][intercept-conf] instructs the appropriate tunneler to intercept any request going to a specific address and send it to ziti. In our case a simple configuration would be:

```json
{
  "addresses":[
    "10.0.2.4"],
  "portRanges":[
    {
      "high":8000,
      "low":8000
    }],
  "protocols":[
    "tcp"],
}

```

It is interesting to note that the two addresses and ports between host and intercept do not have to match. For example, we can make services that are not transparent to the outside world, but the host-intercept pair provides the mapping.

### Service definition

A service definition basically comprises of a series of configurations. In our case, we just define a service comprising of the two configurations above.

### Policy definition

Final step before moving on, is to define a policy for the service. The policy defines which identities can access each service.
Similar to the service configurations, each service policy comprises of two policies:

- Dial policies, which control who can use a service
- Bind policies, which control who can provide a service

Each policy consists of one or more services and one or more identities.
Finally one could use attributes that are assigned to identities and services and basically form a group, in order to make it easier to handle large sets of users/services.

<!-- References -->
[host-conf]: https://openziti.io/docs/learn/core-concepts/config-store/config-type-host-v1/
[intercept-conf]: https://openziti.io/docs/learn/core-concepts/config-store/config-type-intercept-v1/
