# Expansion to new Domains

To expand the Maestro platform to a new domain, a ZTC Client service must be invoked and authenticated against the ZTC Fabric service.
The following steps guide you through this process.

## Server side

- The ZTC network owner must create a unique identity token for the new domain via the ZTC controller. The ZTC network owner shares this identity token with the domain owner.

## Client side

### ZTC Client as Host or Edge router tunneller

- The domain owner stores the identity token with name “ziti_id.jwt“.
- The domain owner creates a docker-compose.yaml file, at the path where the “ziti_id.jwt“ is stored, as described bellow:

```shell
version: "3.9"
services:
    ziti-tun:
        image: openziti/ziti-edge-tunnel
        devices:
            - /dev/net/tun:/dev/net/tun
        volumes:
            - .:/ziti-edge-tunnel
            - /var/run/dbus/system_bus_socket:/var/run/dbus/system_bus_socket
        environment:
            - ZITI_IDENTITY_BASENAME=ziti_id   # define the prefix name of the given identity token with name “ziti_id.jwt“
            - PFXLOG_NO_JSON=true              # suppress JSON logging
        network_mode: host
        privileged: true
```

Then, the ZTC Client service can be invoked as follows:

```bash
docker compose run -d
```

Alternatively, one could directly execute:

```bash
docker run -d  --name ziti-edge-tunnel  --network host  --privileged  --volume ${PWD}:/ziti-edge-tunnel/    --volume "/var/run/dbus/system_bus_socket:/var/run/dbus/system_bus_socket"  --device "/dev/net/tun:/dev/net/tun" --env ZITI_IDENTITY_BASENAME=ziti_id openziti/ziti-edge-tunnel run
```

**_NOTE:_** The above commands should be executed on the same directory where the identity token, “ziti_id.jwt“, file is stored.

**_NOTE:_** If the domain owner wishes to expose services from multiple nodes behind a Local Area Network (LAN), they should opt for the edge router tunneller deployment option. To achieve this, the domain owner should follow the Docker installation instructions provided previously and also configure the ZTC Client service to act as a gateway (edge router tunneller deployment option) to facilitate outbound connections to the ZTC fabric. The configuration should be done as follows:

1. Secondary DNS Server: Configure the edge router tunneller as a secondary DNS server. This setup ensures that devices within the LAN can correctly resolve domain names within the ZTC Fabric.

1. Gateway for 100.64.0.0/10 (ZTC Fabric): Set the edge router tunneller as the default gateway for the IP range 100.64.0.0/10, which corresponds to the ZTC Fabric.

By applying these configurations, the edge router tunneller forwards outbound connections to the ZTC fabric.
