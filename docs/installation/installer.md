# Maestro Installer

Maestro is packaged as a cloud-native service using [Helm][helm-url].

We're using the following helm chart to install Maestro: <https://gitlab.ubitech.eu/maestro-v2/installer/io.maestro.installer>

## Hardware Requirements

For a functional Maestro deployment with Helm, we need a pre-deployed Kubernetes environment.

The Kubernetes environment should contain, at least, a single server or VM (single-node Kubernetes cluster) with the following requirements:

| Compute Requirement | Minimum Value | Recommended Value |
| :------------------ | :------------ | :---------------- |
| Number of CPU cores | 4             | 8                 |
| Main Memory         | 12GB          | 16GB              |
| Storage             | 40GB          | 60GB              |

The environment also needs to have a single interface with Internet access.

## Helm Installation

If Helm is not already installed, follow these instructions:

```bash
wget https://get.helm.sh/helm-v3.13.1-linux-amd64.tar.gz
tar -zxvf helm-v3.13.1-linux-amd64.tar.gz
sudo mv linux-amd64/helm /usr/local/bin/
helm repo add stable https://charts.helm.sh/stable
helm repo update
```

## Longhorn Installation

**_NOTE:_** PVC support is needed if we want to enable PostgreSQL primary data persistence using PVC. In case we don't want to persist the data you can skip PVC Longhorn installation

```bash
curl -sSfL https://raw.githubusercontent.com/longhorn/longhorn/v1.4.2/scripts/environment_check.sh | bash
helm repo add longhorn https://charts.longhorn.io
helm repo update
helm install longhorn longhorn/longhorn --namespace longhorn-system --create-namespace
```

To disable persistence in the Maestro helm chart you need to update the values.yaml file with:

```yaml

### loki section

loki:
  singleBinary:
    persistence:
      enabled: false

### tempo section

tempo:
  persistence:
    enabled: false


### kafka section

kafka:
  controller:
    persistence:
      enabled: false

### postgresql-db section

postgresql-db:
  primary:
    persistence:
      enabled: false

### pgadmin section

pgadmin:
  persistentVolume:
    enabled: false
```

## Create Kubernetes secret to pull from private registry

In order to pull Maestro images from our private registry we need to  create a Kubernetes secret.

Create a  secret with name ```regcred``` and set your ```username``` and ```password``` accordingly.

```bash
kubectl create secret docker-registry regcred --docker-server=https://registry.ubitech.eu/ --docker-username=<username> --docker-password=<password>
```

## Keycloak

The Maestro Orchestrator helm chart needs an operative Keycloak deployment for connection and authorization. We provide two options on how to integrate Keycloak with the Orchestrator chart.

### Keycloak Helm chart

By default, the Orchestrator chart installs bitnami Keycloak chart as a dependency. In this case, the host IP is used as the authentication server.

### External Keycloak option

We can also disable the default Keycloak installation (with `keycloak.setup=false`) and set an existing Keycloak server (with `externalKeycloak.host`). This Keycloak instance can be a chart deployed in the same or a different cluster, or it can even be a Keycloak instance deployed entirely outside K8s.

#### Installation of standalone Keycloak - Skip if Keycloak is already installed

We also provide an option for standalone Keycloak helm chart installation. If you want, you can deploy Keycloak on its own by running:

```bash
helm install keycloak oci://registry-1.docker.io/bitnamicharts/keycloak -f helm_conf_files/values-keycloak.yaml
```

The helm_conf_files folder provides the values.yaml file which is needed for Keycloak to operate with Maestro. Specifically, the realm.json for the TMF realm is provided. We should also note that this Keycloak installation option will deploy a Postgres database, whereas the default Keycloak will use the existing Postgres instance provided by Maestro.

## Installation of Maestro

- Clone the repo

```bash
    git clone https://gitlab.ubitech.eu/maestro-v2/installer/io.maestro.installer.orchestrator.helm.git
```

- Navigate to the `charts/maestro` directory:

```bash
    cd charts/maestro
```

- Build Helm dependencies:

```bash
    helm dep build
```

- Install Maestro Orchestrator using Helm:

```bash
    helm install maestro .
```

**_NOTE:_**

- The above will deploy the full stack of Maestro Orchestrator.

- The deployment will be in the default namespace of Kubernetes with the name "maestro".

**_NOTE:_** You can change Maestro Orchestrator configuration by enabling/disabling supporting services, or change the default values at values.yaml file

### Uninstall Maestro

To uninstall the Maestro Orchestrator deployment, use the following commands:

```bash
helm uninstall maestro
```

Additionally, you have to delete the Persistent Volume Claims (PVC) associated with the "maestro" chart, which are:

```text
NAMESPACE   NAME
default     data-maestro-kafka-controller-0
default     data-maestro-postgresql-db-0
default     maestro-jaeger-all-in-one
default     maestro-pgadmin
```

## Installation Info

### Maestro Microservices

The chart deploys all of the core microservices of Maestro. You may view the information about each core microservice in the Architecture section.

### Supporting services

In addition to the core microservices provided by Maestro, the Orchestrator chart also deploys the following supporting services.

#### Infinispan

[Infinispan](https://infinispan.org/) is a distributed in-memory key/value data store with optional schema. We use it along with the Sonata core deployment.

#### OCI Registry

An [OCI registry](https://helm.sh/docs/topics/registries/) is used in order to store the helm charts created by the Helm Engine. This service is provided by two components: the [Registry Server](https://hub.docker.com/_/registry), which stores the created charts, and the [Registry UI](https://github.com/Joxit/docker-registry-ui), where the user can visualize the created helm charts.

### Maestro supporting charts

#### 1. Kube-prometheus-stack

Installs the kube-prometheus stack, a collection of Kubernetes manifests, Grafana dashboards, and Prometheus rules combined with documentation and scripts to provide easy to operate end-to-end Kubernetes cluster monitoring with Prometheus using the Prometheus Operator.

Chart Info:

```yaml
name: kube-prometheus-stack
version: ~58.0.0
repository: https://prometheus-community.github.io/helm-charts
```

#### 2. Loki

Installs Loki, our logging component, which is also a part of the Grafana stack. Loki is a horizontally scalable, highly available, multi-tenant log aggregation system inspired by Prometheus.

Chart Info:

```yaml
name: loki
version: ~6.3.3
repository: https://grafana.github.io/helm-charts
```

#### 3. PromTail

Installs promtail, which is the agent responsible for gathering logs and sending them to Loki.

Chart Info:

```yaml
name: promtail
version: ~6.15.5
repository: https://grafana.github.io/helm-charts
```

#### 4. Tempo

Installs Grafana Tempo, an open source, easy-to-use, and high-scale distributed tracing backend. Tempo is cost-efficient, requiring only object storage to operate, and is deeply integrated with Grafana, Prometheus, and Loki. It collects traces through our OpenTelemetry Collector, which is described below.

Chart Info:

```yaml
name: tempo
version: ~1.7.2
repository: https://grafana.github.io/helm-charts
```

#### 5. Kafka

Installs an apache kafka component, which is a distributed streaming platform designed to build real-time pipelines and can be used as a message broker or as a replacement for a log aggregation solution for big data applications.

Chart Info:

```yaml
name: kafka
version: ~26.3.0
repository: oci://registry-1.docker.io/bitnamicharts
```

#### 6. Postgresql DB

Installs postgresql database, an open source object-relational database known for reliability and data integrity. ACID-compliant, it supports foreign keys, joins, views, triggers and stored procedures.

Chart Info:

```yaml
name: postgresql  
alias: postgresql-db
version: ~13.2.2
repository: oci://registry-1.docker.io/bitnamicharts
```

#### 7. PgAdmin4

Installs pgadmin4, a management tool for Postgres.

Chart Info:

```yaml
name: pgadmin4
alias: pgadmin
version: ~1.18.2
repository: https://helm.runix.net
```

#### 8. OpenTelemetry Collector

Installs an opentelemetry-collector component, thats offers a vendor-agnostic implementation on how to receive, process and export telemetry data. Operates with Tempo.

Chart Info:

```yaml
name: opentelemetry-collector
alias: otel-collector
version: 0.60.0
repository: https://open-telemetry.github.io/opentelemetry-helm-charts
```

#### 9. Keycloak

Installs Keycloak, our user authorizaton and authentication server.

Chart Info:

```yaml
name: keycloak
version: ~17.3.1
repository: oci://registry-1.docker.io/bitnamicharts
```

#### 10. Kong

Installs Kong component, a cloud-native, platform-agnostic, scalable API Gateway distinguished for its high performance and extensibility via plugins. Requires connection with Postgres DB.

Note: Our Kong deployment is set to false by default.

Chart Info:

```yaml
name: kong
version: ~2.31.0
repository: https://charts.konghq.com
```

#### 11. Mimir

Installs Mimir, an open source, horizontally scalable, highly available, multi-tenant TSDB for long-term storage for Prometheus. Set to false, by default.

Note: Our Mimir deployment is set to false by default.

Chart Info:

```yaml
repository: https://grafana.github.io/helm-charts
version: 5.1.3
name: mimir-distributed
```

<!-- References -->
[helm-url]: https://helm.sh/
