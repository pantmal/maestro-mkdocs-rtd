# <img alt="Swagger" src="../../images/swagger-small.png" width="30" height="30"/> Production Maestro API

To access the latest Maestro APIs, a production instance is deployed through CI/CD.

This production Maestro instance is exposed through a public domain `http://maestro.euprojects.net` where you may check the exposed northbound APIs using [swagger][maestro-prod-domain-swagger].

<!-- References -->
[maestro-prod-domain-swagger]: http://maestro.euprojects.net/tmf-api/q/swagger-ui/
