# Maestro End-to-End Service Orchestrator

![Maestro logo](./images/maestro-logo-large-blue.png)

Maestro is a prototype service orchestrator for managing the lifecycle of end-to-end services atop geo-distributed heterogeneous infrastructures.

## Maestro Ecosystem

Today's infrastructures expand towards the end users, where numerous Internet of Things (IoT) and/or user equipment (UE) devices require connectivity with local (edge) or remote (far in the cloud) services. This connectivity may be often provided via cellular (5G and beyond) networks or low-power IoT networks integrated with multiple geo-distributed (edge and core) cloud infrastructures.

## Why do you need Maestro

Such complex ecosystems pose several challenges in the way service onboarding, deployment, and lifecycle management (LCM) is performed in an end-to-end fashion, mainly because modern services:

- are structured as collections of independently deployable and loosely coupled microservices;
- may span across multiple administrative domains - managed by different owners - who often do not trust each other;
- are often associated with service level agreements (SLAs), which pose strict compute (i.e., CPU, main memory, storage) and network (i.e., latency, throughput, packet loss) requirements.

Maestro is a holistic end-to-end service orchestration platform that aims to bridge these gaps by offering zero-trust multi-domain service orchestration abstractions to various stakeholders. Specifically, Maestro addresses requirements for the following stakeholders:

### Infrastructure Owners

 <input type="checkbox" checked disabled> allows infrastructure owners/providers to register new (private) domains and services under the platform's realm through a programmable zero-trust connectivity (ZTC) fabric.
 <input type="checkbox" checked disabled> manages services across multiple geo-distributed “non-trusted” domains acting as root of trust.

### Service Providers

 <input type="checkbox" checked disabled> allows service providers to package distributed services as if they are centralized, thus moving complexity to the platform.
   <input type="checkbox" checked disabled> offers a single set of service management APIs, no matter how many domains your application expands to.
   <input type="checkbox" checked disabled> supports state-of-the-art service packaging tools (i.e., [Kubernetes][k8s-url], [helm][helm-url], and [docker-compose][docker-comp-url]).
 <input type="checkbox" checked disabled> provides programmable connectivity services across clusters/domains, giving a single-cluster illusion to the users.
 <input type="checkbox" checked disabled> provides a real-time view of the deployed service instances' state.
 <input type="checkbox" checked disabled> provides knobs to change a service instance's runtime state via a service update API and/or real-time policies.

### Relevant Platform Providers

 <input type="checkbox" checked disabled> decouples service and resource management via integration with operations support systems (OSS) using open standardized APIs.
   <input type="checkbox" checked disabled> Maestro deals with service management.
   <input type="checkbox" checked disabled> delegates resource management to OSS (e.g., [ETSI OpenSlice][etsi-osl-url]).
   <input type="checkbox" checked disabled> manages multiple OSS instances.
 <input type="checkbox" checked disabled> integrates with vanilla container orchestration platforms (i.e., [Kubernetes][k8s-url]).

## Maestro Architecture

Maestro comprises of multiple microservices, loosely coupled together via a service bus, as shown in [Figure 1](#maestro-architecture).

![Maestro architecture][maestro-arch]
<center><em>Figure 1: Maestro architecture.</em></center>

## North-Bound APIs

Maestro provides management APIs for products, services, resources, locations, and parties based on open [TMForum (TMF) API specifications][tmf-open-apis] as well as secure management of secrets through [HashiCorp's Vault][vault-url].
All Maestro North-Bound Interfaces (NBIs) are described in the [NBI service][nbi-doc].

### APIs towards an underlying OSS

Maestro leverages the same TMF service and resource APIs to consume compute and network resource services from one or more underlying operations support systems (OSS).
Such services are (i) selected from an OSS service catalog using the [TMF633][tmf-633-url] API, (ii) ordered using the [TMF641][tmf-641-url] API, while (iii) Maestro oversees their runtime management via the [TMF638][tmf-638-url] service inventory API.

Currently, Maestro integrates with the [ETSI OpenSlice][etsi-osl-url] OSS using these APIs for ordering:

- Kubernetes compute resources (K8s-aaS) and
- 5G network resources (5G-aaS).

## Installation Guide

Follow the guidelines [here][maestro-installer-doc] to provision Maestro on a physical or virtual machine.

## Source Code

Maestro's source code will soon be available [here][maestro-src-url].

## How to Contribute

Maestro's development guide is available [here][maestro-dev-doc].

## Web Presense

- [Maestro website][maestro-website-url]
- [Maestro documentation][maestro-docs-url]

<!-- References -->
[maestro-arch]: images/maestro-arch.png "Maestro architecture"

[docker-comp-url]: https://docs.docker.com/compose/
[k8s-url]: https://kubernetes.io/
[helm-url]: https://helm.sh/

[tmf-open-apis]: https://www.tmforum.org/oda/open-apis/directory
[vault-url]: https://www.vaultproject.io/
[nbi-doc]: architecture/nbi.md

[tmf-633-url]: https://tmf-open-api-table-documents.s3.eu-west-1.amazonaws.com/OpenApiTable/TMF633_Service_Catalog/4.0.0/user_guides/TMF633_Service_Catalog_Management_API_v4.0.0_specification.pdf
[tmf-641-url]: https://tmf-open-api-table-documents.s3.eu-west-1.amazonaws.com/OpenApiTable/TMF641_Service_Ordering/4.1.0/user_guides/TMF641_Service_Ordering_Management_API_v4.1.0_specification.pdf
[tmf-638-url]: https://tmf-open-api-table-documents.s3.eu-west-1.amazonaws.com/OpenApiTable/TMF638_Service_Inventory/4.0.0/user_guides/TMF638_Service_Inventory_Management_API_v4.0.0_specification.pdf

[etsi-osl-url]: https://osl.etsi.org/

[maestro-installer-doc]: installation/installer.md
[maestro-src-url]: https://gitlab.ubitech.eu/maestro-v2
[maestro-dev-doc]: development/dev-guide.md
[maestro-website-url]: https://themaestro.ubitech.eu/
[maestro-docs-url]: https://maestro-mkdocs.readthedocs.io
