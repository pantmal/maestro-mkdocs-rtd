# Maestro Development Guide

Table 1 summarizes the services that comprise Maestro along with the programming languages and frameworks used for their development.
Note that for the services based on [Apache Quarkus][quarkus-url], Maestro implements continuous integration with the latest Quarkus releases.

<center><em>Table 1: Maestro services and their programming languages.</em></center>

|   Maestro Service  | Programming Language                                                      | Programming Language Version | Development Framework                                                           |       Status       |
| :----------------- | :------------------------------------------------------------------------ | :--------------------------- | :------------------------------------------------------------------------------ | :----------------- |
| TMF API            | <img alt="Java" src="../../images/icon-java.png" width="60" height="40"/> | 17                           | <img alt="Quarkus" src="../../images/icon-quarkus.png" width="80" height="20"/> | Implemented        |
| TMF Schema         | <img alt="Java" src="../../images/icon-java.png" width="60" height="40"/> | 17                           | <img alt="Quarkus" src="../../images/icon-quarkus.png" width="80" height="20"/> | Implemented        |
| SONATA             | <img alt="Java" src="../../images/icon-java.png" width="60" height="40"/> | 17                           | <img alt="Quarkus" src="../../images/icon-quarkus.png" width="80" height="20"/> | Implemented        |
| Helm Engine        | <img alt="Go" src="../../images/icon-golang.png" width="40" height="40"/> | 1.21.2                       | -                                                                               | Implemented        |
| ZTC Client         | <img alt="Go" src="../../images/icon-golang.png" width="40" height="40"/> | 1.21.2                       | -                                                                               | Implemented        |
| Secrets' Manager   | <img alt="Go" src="../../images/icon-golang.png" width="40" height="40"/> | 1.21.2                       | -                                                                               | Implemented        |
| OSS Client         | <img alt="Java" src="../../images/icon-java.png" width="60" height="40"/> | 17                           | <img alt="Quarkus" src="../../images/icon-quarkus.png" width="80" height="20"/> | Implemented        |
| Telemetry Client   | <img alt="Java" src="../../images/icon-java.png" width="60" height="40"/> | 17                           | <img alt="Quarkus" src="../../images/icon-quarkus.png" width="80" height="20"/> | Under Development  |
| Dashboard Client   | <img alt="Java" src="../../images/icon-java.png" width="60" height="40"/> | 17                           | <img alt="Quarkus" src="../../images/icon-quarkus.png" width="80" height="20"/> | Under Development  |

## Java Quarkus Development Guide

To make yourself comfortable with Java Quarkus, thus being able to contribute to this project, please follow [this][java-quarkus-guide] guide.

## Go Development Guide

To make yourself comfortable with the Go programming language, thus being able to contribute to this project, please follow [this][go-guide] guide.

<!-- References -->
[quarkus-url]: https://quarkus.io
[java-quarkus-guide]: https://quarkus.io/guides/
[go-guide]: https://go.dev/doc/tutorial/getting-started

<!-- Nice repo with markdown badgws: https://github.com/Ileriayo/markdown-badges -->
